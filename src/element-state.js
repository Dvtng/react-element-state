import React, {Component, PropTypes} from 'react';

export default class ElementState extends Component {
    static propTypes = {
        children: PropTypes.func
    };

    static defaultProps = {
        children: function() {}
    };

    state = {
        isHovering: false,
        isActive: false,
        isFocused: false
    };

    eventHandlers = {
        onMouseEnter: () => this.setState({isHovering: true}),
        onMouseLeave: () => this.setState({isHovering: false}),
        onMouseDown: () => this.setState({isActive: true}),
        onMouseUp: () => this.setState({isActive: false}),
        onFocus: () => this.setState({isFocused: true}),
        onBlur: () => this.setState({isFocused: false})
    };

    render() {
        return (
            <span {...this.eventHandlers}>
                {this.props.children(this.state)}
            </span>
        );
    }
}

# react-element-state

```
import ElementState from 'react-element-state';
```

`<ElementState>` is a React component that manages the 3 most common user interaction states,
letting you focus on what to do with them. It provides:

- `isHovering` (true/false) - whether the mouse is hovered over the element
- `isActive` (true/false) - whether the mouse is pressed on the element
- `isFocused` (true/false) - whether the element has the document focus

## Usage

Pass a particular state to control your component:

```
<ElementState>
    {state => <MyButton highlight={state.isHovering}/>}
</ElementState>
```

If the prop names match up, spread all states onto your component:

```
<ElementState>
    {state => <MyButton {...state}/>}
</ElementState>
```

Or choose the states you care about and use them directly:

```
<ElementState>
    {({isFocused, isActive}) => {
        return <button>
            I'm {isFocused ? 'focused' : 'not focused'},
            and {isActive ? 'active' : 'not active'}
        </button>;
    }}
</ElementState>
```

import ElementState from './element-state';
import React from 'react';
import {expect} from 'chai';
import {shallow} from 'enzyme';

describe('ElementState', () => {
    it('handles focus state', () => {
        const el = shallow(
            <ElementState>
                {({isFocused}) => isFocused ? 'focused' : 'not focused'}
            </ElementState>
        );

        expect(el.text()).to.eql('not focused');

        el.simulate('focus');

        expect(el.text()).to.eql('focused');

        el.simulate('blur');

        expect(el.text()).to.eql('not focused');
    });

    it('handles active state', () => {
        const el = shallow(
            <ElementState>
                {({isActive}) => isActive ? 'active' : 'not active'}
            </ElementState>
        );

        expect(el.text()).to.eql('not active');

        el.simulate('mouseDown');

        expect(el.text()).to.eql('active');

        el.simulate('mouseUp');

        expect(el.text()).to.eql('not active');
    });

    it('handles hover state', () => {
        const el = shallow(
            <ElementState>
                {({isHovering}) => isHovering ? 'hovering' : 'not hovering'}
            </ElementState>
        );

        expect(el.text()).to.eql('not hovering');

        el.simulate('mouseEnter');

        expect(el.text()).to.eql('hovering');

        el.simulate('mouseLeave');

        expect(el.text()).to.eql('not hovering');
    });
});
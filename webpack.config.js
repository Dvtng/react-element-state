var path = require('path');

module.exports = {
    context: __dirname,
    entry: {
        example: "./example/index"
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: "[name].bundle.js"
    },
    devtool: '#inline-source-map',
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'react-hot',
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                loader: 'babel',
                query: {
                    cacheDirectory: true
                },
                exclude: /node_modules/
            }
        ]
    }
};

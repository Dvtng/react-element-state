import React from 'react';
import ReactDOM from 'react-dom';
import ElementState from '../src/element-state';

const root = document.getElementById('root');

ReactDOM.render(
    <ElementState>
        {({isActive, isHovering, isFocused}) => {
            const style = {
                background: isActive ? '#f0f0f0' : '#ddd',
                border: `1px solid ${isHovering ? '#38f' : 'transparent'}`,
                padding: 20
            };
            return (
                <button style={style}>I'm stateful</button>
            );
        }}
    </ElementState>,
    root
);
